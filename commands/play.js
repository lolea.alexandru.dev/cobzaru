module.exports = {
  name: "play",
  command(message, args, client) {
    if (!args[0]) {
      message.channel.send("Da zi si ce sa cant mah");
      return;
    }

    if (!message.member.voice.channel) {
      message.channel.send("Baga-te pe un canal ca sa stiu unde cant");
      return;
    }

    client.DisTube.play(message.member.voice.channel, args.join(" "), {
      member: message.member,
      textChannel: message.channel,
      message,
    });
  },
};
